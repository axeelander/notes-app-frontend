import React, {ForwardedRef, useCallback} from 'react'
import Scrollbars from 'react-custom-scrollbars'
import {makeStyles} from 'tss-react/mui'

interface CustomScrollbarProps {
  onScroll?: React.UIEventHandler<any>
  forwardedRef?: ForwardedRef<any>
  style?:  React.CSSProperties
  children: JSX.Element
}

const useStyles = makeStyles()({});

export const CustomScrollbars = ({ onScroll, forwardedRef, style, children }: CustomScrollbarProps) => {
  const { theme } = useStyles()

  const renderThumb = useCallback(
    ({style, ...props}) => {
      return (<div style={{
        ...style,
        backgroundColor: theme.palette.primary.main,
        borderRadius: '5px'
      }} {...props}/>)
    },
    [theme],
  )

  return (
    <Scrollbars
      ref={forwardedRef}
      style={{
        ...style,
        overflow: 'hidden',
      }}
      onScroll={onScroll}
      renderThumbVertical={renderThumb}
    >
      {children}
    </Scrollbars>
);
}

export const CustomScrollbarsVirtualList = React.forwardRef((
  {children, ...props}: CustomScrollbarProps, ref: ForwardedRef<any>) => (
    <CustomScrollbars {...props} forwardedRef={ref}>{children}</CustomScrollbars>
  )
)
