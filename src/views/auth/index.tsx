import {Button, Container, Paper, Stack, TextField, Typography} from '@mui/material'
import React, {useCallback, useState} from 'react'
import {useNavigate} from 'react-router-dom'
import {login, register} from '../../features/auth/userSlice'
import {useAppDispatch, useAppSelector} from '../../store'

export function Login() {
  const [userName, setUserName] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState<string | null>(null)
  const { state: loginState } = useAppSelector(state => state.user)
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const validateFields = useCallback(
    () => {
      if (userName.length === 0) {
        setError('Username cannot be empty.')
        return false
      }
      if (password.length === 0) {
        setError('Password cannot be empty.')
        return false
      }
      setError(null)
      return true
    },
    [userName, password],
  )

  const handleSignInClick = useCallback(
    async () => {
      if (validateFields()) {
        if (loginState === 'offline') {
          try {
            await dispatch(login({
              name: userName,
              password,
            }))
            navigate('/')
          } catch (e) {
            setError(`Failed to sign in: ${e}`)
          }
        } else if (loginState === 'online') {
          setError('You are already online.')
        }
      }
    },
    [dispatch, loginState, navigate, password, userName, validateFields],
  )

  const handleSignUpClick = useCallback(
    async () => {
      if (validateFields()) {
        if (loginState === 'offline') {
          try {
            await dispatch(register({
              name: userName,
              password,
            }))
            await dispatch(login({
              name: userName,
              password,
            }))
            navigate('/')
          } catch (e) {
            setError(`Failed to sign up: ${e}`)
          }
        } else if (loginState === 'online') {
          setError('You are already online.')
        }
      }
    },
    [dispatch, loginState, navigate, password, userName, validateFields],
  )

  return (
    <Stack justifyContent='center' alignItems='center' sx={{ width: '100%', height: '100%' }}>
      <Container maxWidth="sm">
        <Paper elevation={6} sx={{
          padding: '10px',
        }}>
          <Typography sx={{
            fontWeight: 'bold',
            mb: '10px',
          }}>Authentication</Typography>
          <TextField
            required
            fullWidth
            margin='normal'
            id='login-name'
            label='User Name'
            value={userName}
            onChange={e => setUserName(e.target.value)}
          />
          <TextField
            required
            fullWidth
            type='password'
            margin='normal'
            id='login-password'
            label='Password'
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
          {error
            ? <Stack direction='row' justifyContent='center' alignItems='center' sx={{
                mt: '10px',
              }}>
                <Typography sx={{color: 'error.main'}}>{error}</Typography>
              </Stack>
            : null
          }
          <Stack direction='row' justifyContent='center' alignItems='center' sx={{
            mt: '10px',
          }}>
            <Button variant='contained' color='primary' onClick={handleSignInClick}>Sign In</Button>
            <Typography sx={{mx: '5px'}}>or</Typography>
            <Button variant='outlined' onClick={handleSignUpClick}>Sign Up</Button>
          </Stack>
        </Paper>
      </Container>
    </Stack>
  )
}
