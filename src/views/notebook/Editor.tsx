import {markdown} from '@codemirror/lang-markdown'
import {Article, Image, Save, VerticalSplit} from '@mui/icons-material'
import {Divider, Grid, IconButton, Paper, Toolbar, Typography} from '@mui/material'
import CodeMirror from '@uiw/react-codemirror'
import 'katex/dist/katex.min.css'
import React, {useCallback, useEffect, useMemo, useState} from 'react'
import {useHotkeys} from 'react-hotkeys-hook'
import ReactMarkdown from 'react-markdown'
import {useParams} from 'react-router-dom'
import {Prism as SyntaxHighlighter} from 'react-syntax-highlighter'
import {materialDark, materialLight} from 'react-syntax-highlighter/dist/esm/styles/prism'
import AutoSizer from 'react-virtualized-auto-sizer'
import rehypeKatex from 'rehype-katex'
import remarkGfm from 'remark-gfm'
import remarkMath from 'remark-math'
import {changeEditorMode} from '../../features/config/configSlice'
import {fetchPageContentById, updatePageContent} from '../../features/notebook/pageSlice'
import {findPageById} from '../../services/notebookService'
import {useAppDispatch, useAppSelector} from '../../store'
import {useMarkdownStyles} from '../../styles'

export function Editor() {
  const { ...params } = useParams()
  const [lastPageId, setLastPageId] = useState<number | undefined>(undefined)
  const pageId = params['pageId'] ? Number(params['pageId']) : undefined
  const [content, setContent] = useState('')
  const { contentById, loadStateById } = useAppSelector(state => state.page)
  const { editorMode: mode } = useAppSelector(store => store.config)
  const { books } = useAppSelector(state => state.book)
  const { theme } = useAppSelector(state => state.config)
  const [lastSavedTime, setLastSavedTime] = useState(new Date(0))
  const dispatch = useAppDispatch()
  const mdStyles = useMarkdownStyles()

  //FIXME: Codes in callback would not be executed
  useHotkeys('ctrl+s', () => {}, {
    enabled: true,
    filter: e => {
      if (pageId) {
        dispatch(updatePageContent({
          id: pageId,
          content,
        }))
        setLastSavedTime(new Date())
      }
      e.preventDefault()
      return true
    }
  })

  const pageTitle = useMemo(() => pageId !== undefined ? findPageById(books, pageId)?.title ?? '' : '', [books, pageId]);

  useEffect(() => {
    if (pageId !== lastPageId) {
      // Page were switching
      if (lastPageId !== undefined) {
        // Save last page to prevent changes loss
        console.log('save when switching...')
        dispatch(updatePageContent({
          id: lastPageId,
          content,
        }))
      }
      setLastPageId(pageId)
    }
  }, [content, dispatch, lastPageId, pageId])

  useEffect(() => {
    if (pageId !== undefined) {
      const state = loadStateById[pageId]
      // Load page content
      if (state === 'loaded' && content !== contentById[pageId]) {
        console.log('set loaded content...')
        setContent(contentById[pageId] || '')
      } else if(state === undefined) {
        dispatch(fetchPageContentById(pageId))
      }
    }
  }, [dispatch, loadStateById, pageId])

  useEffect(() => {
    // Save the editing page every minute
    if (pageId !== undefined && pageId === lastPageId
      && (new Date().getTime() - lastSavedTime.getTime()) > 60 * 1000) {
      console.log('autosaving...')
      dispatch(updatePageContent({
        id: pageId,
        content,
      }))
      setLastSavedTime(new Date())
    }
  }, [dispatch, lastSavedTime, pageId, content, lastPageId])

  const onUpdate = useCallback(
    (value: string) => {
      setContent(value)
    },
    [],
  )

  const onSave = useCallback(
    async () => {
      if (pageId) {
        await dispatch(updatePageContent({
          id: pageId,
          content,
        }))
        setLastSavedTime(new Date())
      }
    },
    [content, dispatch, pageId],
  )

  //TODO: Show the message of failure

  return (
    <Paper elevation={12} sx={{height: '100%', overflow: 'hidden', transition: 'width 300ms linear'}}>
      <Grid container direction={'column'} sx={{height: '100%', overflow: 'hidden'}}>
        <Grid item sm={'auto'}>
          <Toolbar variant='dense' disableGutters sx={{height: '20px'}}>
            <IconButton onClick={onSave}><Save/></IconButton>
            <Typography sx={{
              fontWeight: 'bold',
              flexGrow: 1,
              textAlign: 'center',
            }}>{pageTitle}</Typography>
            <IconButton onClick={() => dispatch(changeEditorMode('text'))}><Article/></IconButton>
            <IconButton onClick={() => dispatch(changeEditorMode('preview'))}><Image/></IconButton>
            <IconButton onClick={() => dispatch(changeEditorMode('both'))}><VerticalSplit/></IconButton>
          </Toolbar>
        </Grid>
        <Divider/>
        <Grid item sm={true} sx={{height: '100%', overflow: 'hidden'}}>
          <Grid container sx={{height: '100%', overflow: 'hidden'}}>
            {mode !== 'preview' &&
              <Grid item sm={true}>
                <AutoSizer>
                  {({height, width}) =>
                    <CodeMirror
                      value={content}
                      width={`${width}px`}
                      height={`${height}px`}
                      theme={theme}
                      extensions={[
                        markdown(),
                      ]}
                      onChange={onUpdate}
                    />
                  }
                </AutoSizer>
              </Grid>}
            {mode !== 'text' &&
              <Grid item sm={true} className={mdStyles.classes.markdownBody} sx={{height: '100%', overflow: 'auto'}}>
                <ReactMarkdown
                  remarkPlugins={[remarkMath, remarkGfm]}
                  rehypePlugins={[() => rehypeKatex({strict: false})]}
                  components={{
                    code({node, inline, className, children, ...props}) {
                      const match = /language-(\w+)/.exec(className || '')
                      return !inline && match ? (
                        <SyntaxHighlighter
                          className={mdStyles.classes.code}
                          children={String(children).replace(/\n$/, '')}
                          style={theme === 'dark' ? materialDark : materialLight}
                          language={match[1]}
                          PreTag="div"
                          {...props}
                        />
                      ) : (
                        <code className={className} {...props}>
                          {children}
                        </code>
                      )
                    }
                  }}
                >{content}</ReactMarkdown>
              </Grid>}
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  )
}

export default Editor
