import {Menu} from '@mui/icons-material'
import {Backdrop, Box, CircularProgress, IconButton, Stack} from '@mui/material'
import React, { Suspense } from 'react'
import {switchNotebookListPanel} from '../../features/config/configSlice'
import {useAppDispatch, useAppSelector} from '../../store'
import {ListPanel} from './ListPanel'

const Editor = React.lazy(() => import('./Editor'))

export function NotebookAppBarWidgets() {
  const dispatch = useAppDispatch()

  return (
    <IconButton onClick={() => dispatch(switchNotebookListPanel())}><Menu/></IconButton>
  )
}

export function Notebook() {
  const { showNotebookListPanel } = useAppSelector(store => store.config)

  return (
    <Box sx={{ width: '100%', height: '100%' }}>
      <Stack direction='row' spacing={2} sx={{ height: '100%', px: '10px' }}>
        <Box sx={{
          height: '100%',
          width: showNotebookListPanel ? '25%' : '0',
          transition: 'width 300ms linear',
        }}>
          <ListPanel/>
        </Box>
        <Box sx={{flexGrow: 1, transition: 'width 300ms linear'}}>
          <Suspense fallback={<Backdrop open={true}><CircularProgress color="inherit" /></Backdrop>}>
            <Editor/>
          </Suspense>
        </Box>
      </Stack>
    </Box>
  )
}
