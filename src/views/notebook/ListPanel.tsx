import {Add, ArrowBack, Delete, Edit} from '@mui/icons-material'
import {
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  ListItem,
  ListItemButton,
  ListItemText,
  Paper,
  TextField,
  Toolbar,
  Typography,
} from '@mui/material'
import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react'
import {useNavigate, useParams} from 'react-router-dom'
import AutoSizer from 'react-virtualized-auto-sizer'
import {FixedSizeList, ListChildComponentProps} from 'react-window'
import {makeStyles} from 'tss-react/mui'
import {CustomScrollbarsVirtualList} from '../../components/CustomScrollbar'
import {
  createNewBook,
  createNewPage,
  deleteBook,
  deletePage,
  fetchBooks,
  renameBook,
  renamePage,
} from '../../features/notebook/bookSlice'
import {Book, findBookById, getAllPages, Page} from '../../services/notebookService'
import {useAppDispatch, useAppSelector} from '../../store'

interface CreateBookDialogProps {
  open: boolean
  setOpen: React.Dispatch<React.SetStateAction<boolean>>
}

function CreateBookDialog({ open, setOpen }: CreateBookDialogProps) {
  const dispatch = useAppDispatch()
  const [title, setTitle] = useState('')
  const [error, setError] = useState<string | null>(null)
  const [waiting, setWaiting] = useState(false)

  const handleCreateClick = useCallback(
    async () => {
      if (title === '') {
        setError('Title cannot be empty.')
        return
      }
      setError(null)
      setWaiting(true)
      await dispatch(createNewBook(title)).unwrap()
      setWaiting(false)
      setOpen(false)
      setTitle('')
      setError(null)
    },
    [dispatch, title, setOpen],
  )

  const handleCancelClick = useCallback(
    () => {
      setOpen(false)
    },
    [setOpen],
  )

  return (
    <Dialog open={open} fullWidth={true}>
      <DialogTitle>Create Book</DialogTitle>
      <DialogContent>
        {
          waiting
            ? <CircularProgress />
            : <TextField
                required
                fullWidth
                autoFocus
                margin='dense'
                variant='standard'
                id='new-book-title'
                label='Book Title'
                value={title}
                onChange={e => setTitle(e.target.value)}
                error={error !== null}
                helperText={error || 'Please input the title of new book'}
              />
        }
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancelClick} disabled={waiting}>Cancel</Button>
        <Button onClick={handleCreateClick} disabled={waiting}>Create</Button>
      </DialogActions>
    </Dialog>
  )
}

interface CreatePageDialogProps {
  open: boolean
  setOpen: React.Dispatch<React.SetStateAction<boolean>>
  bookId: number
}

function CreatePageDialog({ open, setOpen, bookId }: CreatePageDialogProps) {
  const dispatch = useAppDispatch()
  const [title, setTitle] = useState('')
  const [error, setError] = useState<string | null>(null)
  const [waiting, setWaiting] = useState(false)

  const handleCreateClick = useCallback(
    async () => {
      if (title === '') {
        setError('Title cannot be empty.')
        return
      }
      setError(null)
      setWaiting(true)
      await dispatch(createNewPage({
        bookId,
        title: title,
      })).unwrap()
      setWaiting(false)
      setOpen(false)
      setTitle('')
      setError(null)
    },
    [title, dispatch, bookId, setOpen],
  )

  const handleCancelClick = useCallback(
    () => {
      setOpen(false)
    },
    [setOpen],
  )

  return (
    <Dialog open={open} fullWidth={true}>
      <DialogTitle>Create Page</DialogTitle>
      <DialogContent>
        {
          waiting
            ? <CircularProgress />
            : <TextField
              required
              fullWidth
              autoFocus
              margin='dense'
              variant='standard'
              id='new-page-title'
              label='Page Title'
              value={title}
              onChange={e => setTitle(e.target.value)}
              error={error !== null}
              helperText={error || 'Please input the title of new page'}
            />
        }
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancelClick} disabled={waiting}>Cancel</Button>
        <Button onClick={handleCreateClick} disabled={waiting}>Create</Button>
      </DialogActions>
    </Dialog>
  )
}

interface RenameBookDialogProps {
  renamingBook: Book | undefined
  setRenamingBook: React.Dispatch<React.SetStateAction<Book | undefined>>
}

function RenameBookDialog({ renamingBook, setRenamingBook }: RenameBookDialogProps) {
  const dispatch = useAppDispatch()
  const [title, setTitle] = useState('')
  const [error, setError] = useState<string | null>(null)
  const [waiting, setWaiting] = useState(false)

  const handleSaveClick = useCallback(
    async () => {
      if (title === '') {
        setError('Title cannot be empty.')
        return
      }
      setError(null)
      setWaiting(true)
      try {
        await dispatch(renameBook({
          id: renamingBook!.id,
          title: title,
        })).unwrap()
        setTitle('')
        setError(null)
      } finally {
        setWaiting(false)
      }
      setRenamingBook(undefined)
    },
    [title, setRenamingBook, dispatch, renamingBook],
  )

  const handleCancelClick = useCallback(
    () => {
      setRenamingBook(undefined)
    },
    [setRenamingBook],
  )

  if (renamingBook === undefined) return null

  return (
    <Dialog open={true} fullWidth={true}>
      <DialogTitle>Rename Book</DialogTitle>
      <DialogContent>
        {
          waiting
            ? <CircularProgress />
            : <TextField
              required
              fullWidth
              autoFocus
              margin='dense'
              variant='standard'
              id='rename-book-title'
              label='Book Title'
              value={title}
              onChange={e => setTitle(e.target.value)}
              error={error !== null}
              helperText={error || 'Please input the new title of this book'}
            />
        }
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancelClick} disabled={waiting}>Cancel</Button>
        <Button onClick={handleSaveClick} disabled={waiting}>Save</Button>
      </DialogActions>
    </Dialog>
  )
}

interface RenamePageDialogProps {
  renamingPage: Page | undefined
  setRenamingPage: React.Dispatch<React.SetStateAction<Page | undefined>>
}

function RenamePageDialog({ renamingPage, setRenamingPage }: RenamePageDialogProps) {
  const dispatch = useAppDispatch()
  const [title, setTitle] = useState('')
  const [error, setError] = useState<string | null>(null)
  const [waiting, setWaiting] = useState(false)

  const handleSaveClick = useCallback(
    async () => {
      if (title === '') {
        setError('Title cannot be empty.')
        return
      }
      setError(null)
      setWaiting(true)
      try {
        await dispatch(renamePage({
          id: renamingPage!.id,
          title: title,
        })).unwrap()
        setTitle('')
        setError(null)
      } finally {
        setWaiting(false)
      }
      setRenamingPage(undefined)
    },
    [title, setRenamingPage, dispatch, renamingPage],
  )

  const handleCancelClick = useCallback(
    () => {
      setRenamingPage(undefined)
    },
    [setRenamingPage],
  )

  if (renamingPage === undefined) return null

  return (
    <Dialog open={true} fullWidth={true}>
      <DialogTitle>Rename Page</DialogTitle>
      <DialogContent>
        {
          waiting
            ? <CircularProgress />
            : <TextField
              required
              fullWidth
              autoFocus
              margin='dense'
              variant='standard'
              id='rename-page-title'
              label='Page Title'
              value={title}
              onChange={e => setTitle(e.target.value)}
              error={error !== null}
              helperText={error || 'Please input the new title of this page'}
            />
        }
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancelClick} disabled={waiting}>Cancel</Button>
        <Button onClick={handleSaveClick} disabled={waiting}>Save</Button>
      </DialogActions>
    </Dialog>
  )
}

interface DeleteBookDialogProps {
  deletingBook: Book | undefined
  setDeletingBook: React.Dispatch<React.SetStateAction<Book | undefined>>
}

function DeleteBookDialog({deletingBook, setDeletingBook}: DeleteBookDialogProps) {
  const dispatch = useAppDispatch()

  const handleDeleteClick = useCallback(
    async () => {
      await dispatch(deleteBook(deletingBook!.id))
      setDeletingBook(undefined)
    },
    [deletingBook, setDeletingBook, dispatch],
  )

  const handleCancelClick = useCallback(
    () => {
      setDeletingBook(undefined)
    },
    [setDeletingBook],
  )

  if (deletingBook === undefined) {
    return null
  }

  return (
    <Dialog open={true}>
      <DialogTitle>Deletion Confirm</DialogTitle>
      <DialogContent>
        You are deleting book {deletingBook?.title}, do you want to continue?
      </DialogContent>
      <DialogActions>
        <Button color={'error'} onClick={handleDeleteClick}>Delete</Button>
        <Button onClick={handleCancelClick}>Cancel</Button>
      </DialogActions>
    </Dialog>
  )
}

interface DeletePageDialogProps {
  deletingPage: Page | undefined
  setDeletingPage: React.Dispatch<React.SetStateAction<Page | undefined>>
}

function DeletePageDialog({deletingPage, setDeletingPage}: DeletePageDialogProps) {
  const dispatch = useAppDispatch()

  const handleDeleteClick = useCallback(
    async () => {
      await dispatch(deletePage(deletingPage!.id))
      setDeletingPage(undefined)
    },
    [deletingPage, setDeletingPage, dispatch],
  )

  const handleCancelClick = useCallback(
    () => {
      setDeletingPage(undefined)
    },
    [setDeletingPage],
  )

  if (deletingPage === undefined) {
    return null
  }

  return (
    <Dialog open={true}>
      <DialogTitle>Deletion Confirm</DialogTitle>
      <DialogContent>
        You are deleting page {deletingPage?.title}, do you want to continue?
      </DialogContent>
      <DialogActions>
        <Button color={'error'} onClick={handleDeleteClick}>Delete</Button>
        <Button onClick={handleCancelClick}>Cancel</Button>
      </DialogActions>
    </Dialog>
  )
}

const useListPanelStyle = makeStyles<void,
  'listItemAction'>()(
  (theme, _params, classes) => ({
    listItem: {
      [`&:hover .${classes.listItemAction}`]: {
        display: 'unset',
        opacity: 1,
      },
    },
    listItemAction: {
      display: 'none',
      opacity: 0,
      transition: theme.transitions.create('opacity'),
    },
  }))

export function ListPanel() {
  const [bookId, setBookId] = useState<number | undefined>(undefined)
  const [pageId, setPageId] = useState<number | undefined>(undefined)
  const [atBookId, setAtBookId] = useState<number | undefined>(undefined)
  const { ...params } = useParams()
  const routePageId = params['pageId'] ? Number(params['pageId']) : undefined

  const navigate = useNavigate()
  const { books, loadState } = useAppSelector(state => state.book)
  const dispatch = useAppDispatch()
  const { classes } = useListPanelStyle()

  useEffect(() => {
    async function inner() {
      if (loadState === 'notLoaded') {
        await dispatch(fetchBooks()).unwrap()
      }
      if (routePageId) {
        const book = books.find(b => b.pages.find(p => p.id === routePageId))
        if (book && bookId === undefined) {
          setPageId(routePageId)
          setAtBookId(routePageId)
          setBookId(book.id)
        }
      }
    }
    inner()
  })

  const pages = useMemo(() => bookId
    ? getAllPages(books, bookId)
    : [], [bookId, books])

  const listRef = useRef<any>()
  const outerRef = useRef()

  const selectBook = useCallback(
    (bookId) => {
      setBookId(bookId)
      setAtBookId(bookId)
    },
    [],
  )

  const selectPage = useCallback(
    (pageId) => {
      setPageId(pageId)
      navigate(`/notebook/${pageId}`)
    },
    [navigate],
  )

  const goBack = useCallback(
    () => {
      setAtBookId(undefined)
    },
    [],
  )

  const [deletingBook, setDeletingBook] = useState<Book | undefined>()
  const [renamingBook, setRenamingBook] = useState<Book | undefined>()
  const renderBookRow = useCallback(
    ({style, index}: ListChildComponentProps) => {
      const book = books[index]

      return (
        <ListItem style={style} className={classes.listItem} key={book.id}>
          <ListItemButton onClick={() => {selectBook(book.id)}} selected={bookId === book.id}>
            <ListItemText>{book.title}</ListItemText>
            <IconButton
              className={classes.listItemAction}
              size={'small'}
              onClick={e => {setRenamingBook(book); e.stopPropagation()}}
            ><Edit fontSize={'small'}/></IconButton>
            <IconButton
              className={classes.listItemAction}
              size={'small'}
              onClick={e => {setDeletingBook(book); e.stopPropagation()}}
            ><Delete fontSize={'small'}/></IconButton>
          </ListItemButton>
        </ListItem>
      )
    },
    [bookId, books, selectBook, classes],
  )

  const [deletingPage, setDeletingPage] = useState<Page | undefined>()
  const [renamingPage, setRenamingPage] = useState<Page | undefined>()
  const renderPageRow = useCallback(
    ({style, index}: ListChildComponentProps) => {
      const page = pages[index]

      return (
        <ListItem style={style} className={classes.listItem} key={page.id}>
          <ListItemButton onClick={() => {selectPage(page.id)}} selected={pageId === page.id}>
            <ListItemText>{page.title}</ListItemText>
            <IconButton
              className={classes.listItemAction}
              size={'small'}
              onClick={e => {setRenamingPage(page); e.stopPropagation()}}
            ><Edit fontSize={'small'}/></IconButton>
            <IconButton
              className={classes.listItemAction}
              size={'small'}
              onClick={e => {setDeletingPage(page); e.stopPropagation()}}
            ><Delete fontSize={'small'}/></IconButton>
          </ListItemButton>
        </ListItem>
      )
    },
    [classes, pageId, pages, selectPage],
  )

  const [createBookModalOpen, setCreateBookModalOpen] = useState(false)
  const [createPageModalOpen, setCreatePageModalOpen] = useState(false)

  const handleAddClick = useCallback(
    () => {
      if (atBookId) {
        setCreatePageModalOpen(true)
      } else {
        setCreateBookModalOpen(true)
      }
    },
    [atBookId],
  )

  return (
    <Paper elevation={12} sx={{ height: '100%' }}>
      <Grid container direction={'column'} sx={{ height: '100%', overflow: 'hidden' }}>
        <Grid item sm={'auto'} sx={{width: '100%', overflow: 'hidden'}}>
          <Toolbar variant='dense' disableGutters sx={{
            paddingLeft: '5px',
            bgcolor: 'leveledBg.level3',
          }}>
            {atBookId
              ? <IconButton onClick={goBack}><ArrowBack/></IconButton>
              : <Typography sx={{
                marginLeft: '10px',
                fontWeight: 'bold',
                flexGrow: '1',
              }}>Books</Typography>
            }
            {atBookId
              ? <Typography sx={{
                fontWeight: 'bold',
                flexGrow: '1',
              }}>{findBookById(books, atBookId)?.title}</Typography>
              : null
            }
            <IconButton onClick={handleAddClick}><Add/></IconButton>
          </Toolbar>
        </Grid>
        <Grid item sm={true}>
          <AutoSizer>
            {({ height, width }) =>
              <FixedSizeList
                ref={listRef}
                className={'List'}
                width={width}
                height={height}
                itemCount={atBookId ? pages.length : books.length}
                itemSize={50}
                outerElementType={CustomScrollbarsVirtualList}
                outerRef={outerRef}
              >
                {atBookId ? renderPageRow : renderBookRow}
              </FixedSizeList>
            }
          </AutoSizer>
        </Grid>
      </Grid>
      <CreateBookDialog open={createBookModalOpen} setOpen={setCreateBookModalOpen}/>
      { atBookId ? <CreatePageDialog open={createPageModalOpen} setOpen={setCreatePageModalOpen} bookId={atBookId}/> : null }

      <RenameBookDialog renamingBook={renamingBook} setRenamingBook={setRenamingBook}/>
      <RenamePageDialog renamingPage={renamingPage} setRenamingPage={setRenamingPage}/>

      <DeleteBookDialog deletingBook={deletingBook} setDeletingBook={setDeletingBook}/>
      <DeletePageDialog deletingPage={deletingPage} setDeletingPage={setDeletingPage}/>
    </Paper>
  )
}
