import {configureStore} from '@reduxjs/toolkit'
import {TypedUseSelectorHook, useDispatch, useSelector} from 'react-redux'
import {userReducer} from './features/auth/userSlice'
import {configReducer} from './features/config/configSlice'
import {bookReducer} from './features/notebook/bookSlice'
import {pageReducer} from './features/notebook/pageSlice'

export const store = configureStore({
  reducer: {
    config: configReducer,
    book: bookReducer,
    page: pageReducer,
    user: userReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>()
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
