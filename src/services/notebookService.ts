import {
  createBookAPI,
  createPageAPI,
  deleteBookAPI,
  deletePageAPI,
  getBooksAPI,
  getPageContentAPI,
  renameBookAPI,
  renamePageAPI,
  updatePageAPI,
} from '../api'

export interface INotebookService {
  getAllBooks(): Promise<Book[]>
  createBook(dto: CreateBookDto): Promise<Book>
  renameBook(dto: RenameBookDto): Promise<void>
  deleteBook(id: number): Promise<void>

  createPage(dto: CreatePageDto): Promise<Page>
  renamePage(dto: RenamePageDto): Promise<void>
  deletePage(id: number): Promise<void>

  getPageContent(id: number): Promise<string>
  updatePageContent(dto: UpdatePageDto): Promise<void>
}

export interface Book {
  id: number
  title: string
  pages: Page[]
  createAt: number
}

export interface CreateBookDto {
  title: string
}

export interface RenameBookDto {
  id: number
  title: string
}

export interface Page {
  id: number
  title: string
  createAt: number
  editAt: number
}

export interface CreatePageDto {
  bookId: number
  title: string
}

export interface RenamePageDto {
  id: number
  title: string
}

export interface UpdatePageDto {
  id: number
  content: string
}

export class BookNotFoundError extends Error {
  constructor(id: number) {
    super(`Book ${id} doesn't exist.`)
  }
}

export class PageNotFoundError extends Error {
  constructor(id: number) {
    super(`Page ${id} doesn't exist.`)
  }
}

class NotebookService implements INotebookService {
  async createBook(dto: CreateBookDto) {
    return await createBookAPI(dto)
  }

  async createPage(dto: CreatePageDto) {
    return await createPageAPI(dto)
  }

  async deleteBook(id: number) {
    await deleteBookAPI({
      id
    })
  }

  async deletePage(id: number) {
    await deletePageAPI({
      id
    })
  }

  async getAllBooks() {
    return await getBooksAPI()
  }

  async getPageContent(id: number) {
    return (await getPageContentAPI(id)).content
  }

  async renameBook(dto: RenameBookDto) {
    await renameBookAPI(dto)
  }

  async renamePage(dto: RenamePageDto) {
    await renamePageAPI(dto)
  }

  async updatePageContent(dto: UpdatePageDto) {
    await updatePageAPI(dto)
  }
}

export function findBookById(books: Book[], id: number) {
  return books.find(b => b.id === id)
}

export function findPageById(books: Book[], id: number) {
  return books.flatMap(b => b.pages).find(p => p.id === id)
}

export function getAllPages(books: Book[], id: number | undefined) {
  if (id) {
    const book = findBookById(books, id)
    return book ? book.pages : []
  } else {
    return books.flatMap(b => b.pages)
  }
}

export const notebookService: INotebookService = new NotebookService()
