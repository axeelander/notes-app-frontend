import faker from '@faker-js/faker'
import {getUnixTime} from 'date-fns'
import {wait} from '../../utils'
import {
  Book,
  BookNotFoundError,
  CreateBookDto,
  CreatePageDto,
  findBookById,
  findPageById,
  INotebookService,
  Page,
  RenameBookDto,
  RenamePageDto,
  UpdatePageDto,
} from '../notebookService'


class MockNotebookService implements INotebookService {
  books: Book[] = []
  bookSerial = 0
  pageSerial = 0

  constructor() {
    let _this = this

    function seedBooks() {
      let books: Book[] = []
      for (let i = 0; i < 40; i++) {
        books.push({
          id: _this.bookSerial++,
          title: `${faker.word.adjective()} ${faker.word.noun()}`,
          pages: seedPages(Math.fround(Math.random() * 20)),
          createAt: getUnixTime(faker.date.past()),
        })
      }
      return books
    }

    function seedPages(count: number) {
      let pages: Page[] = []
      for (let i = 0; i < count; i++) {
        pages.push({
          id: _this.pageSerial++,
          title: `${faker.word.adjective()} ${faker.word.noun()}`,
          createAt: getUnixTime(faker.date.past()),
          editAt: getUnixTime(faker.date.recent()),
        })
      }
      return pages
    }

    this.books = seedBooks()
  }

  async refresh() {
    await wait(180)
  }

  async createBook(dto: CreateBookDto) {
    let newBook = {
      id: this.bookSerial++,
      title: dto.title,
      pages: [],
      createAt: getUnixTime(new Date()),
    }
    this.books.push(newBook)
    await wait(100)
    return JSON.parse(JSON.stringify(newBook))
  }

  async createPage(dto: CreatePageDto) {
    let newPage: Page = {
      id: this.pageSerial++,
      title: dto.title,
      createAt:  getUnixTime(new Date()),
      editAt: getUnixTime(new Date()),
    }
    let book = findBookById(this.books, dto.bookId)
    if (book) {
      book.pages.push(newPage)
      await wait(100)
      return newPage
    } else {
      throw new BookNotFoundError(dto.bookId)
    }
  }

  async deleteBook(id: number) {
    this.books = this.books.filter(b => b.id !== id)
    await wait(100)
  }

  async deletePage(id: number) {
    this.books.forEach(b => b.pages = b.pages.filter(p => p.id !== id))
    await wait(100)
  }

  async getAllBooks() {
    await wait(180)
    return JSON.parse(JSON.stringify(this.books))
  }

  async renameBook(dto: RenameBookDto) {
    let book = findBookById(this.books, dto.id)
    if (book) {
      book.title = dto.title
    } else {
      throw new BookNotFoundError(dto.id)
    }
    await wait(100)
  }

  async renamePage(dto: RenamePageDto) {
    let page = findPageById(this.books, dto.id)
    if (page) {
      page.title = dto.title
    } else {
      throw new BookNotFoundError(dto.id)
    }
    await wait(100)
  }

  async getPageContent(id: number) {
    await wait(200)
    return findPageById(this.books, id) ? faker.lorem.paragraphs() : ''
  }

  async updatePageContent(dto: UpdatePageDto) {
    await wait(200)
  }
}
