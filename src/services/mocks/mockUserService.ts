import {wait} from '../../utils'
import {IUserService, LoginReqDto, RegisterReqDto} from '../userService'

class MockUserService implements IUserService {
  async getUserInfo() {
    await wait(50)
    return {
      id: 114514,
      name: 'foo',
      avatarUrl: 'https://via.placeholder.com/64',
    }
  }

  async login(dto: LoginReqDto) {
    await wait(200)
    return {
      id: 114514,
      name: dto.name,
      avatarUrl: 'https://via.placeholder.com/64',
    }
  }

  async logout() {
    await wait(100)
  }

  async register(dto: RegisterReqDto) {
    await wait(200)
    return {
      id: 114514,
      name: dto.name,
      avatarUrl: 'https://via.placeholder.com/64',
    }
  }
}
