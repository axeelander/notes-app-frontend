import {axiosInstance, getUserInfoAPI, loginAPI, registerAPI} from '../api'

export interface IUserService {
  login(dto: LoginReqDto): Promise<UserInfo>
  logout(): Promise<void>
  register(dto: RegisterReqDto): Promise<UserInfo>
  getUserInfo(): Promise<UserInfo>
}

export interface UserInfo {
  id: number
  name: string
  avatarUrl: string
}

export interface LoginReqDto {
  name: string
  password: string
}

export interface LoginResDto {
  token: string
}

export interface RegisterReqDto {
  name: string
  password: string
}

class UserService implements IUserService {
  async getUserInfo() {
    return await getUserInfoAPI()
  }

  async login(dto: LoginReqDto) {
    let { token } = await loginAPI(dto)
    localStorage.setItem('authToken', token)
    if (axiosInstance) {
      axiosInstance.defaults.headers.common["Authorization"] = `Bearer ${token}`
    }
    return await this.getUserInfo()
  }

  async logout() {
    localStorage.clear()
  }

  async register(dto: RegisterReqDto) {
    return await registerAPI(dto)
  }

}

export const userService: IUserService = new UserService()
