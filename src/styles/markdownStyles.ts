import {makeStyles} from 'tss-react/mui'

export const useMarkdownStyles = makeStyles()(
  (theme, _, classes) => ({
    markdownBody: {
      padding: '1em',
      position: 'relative',
      width: '0', // hack to prevent overflow
      blockquote: {
        margin: 0,
        borderLeft: `5px solid ${theme.palette.primary.main}`,
        fontStyle: 'italic',
        padding: '1.33em',
        textAlign: 'left',
      },
      img: {
        maxWidth: '100%',
      },
      pre: {
        backgroundImage: `linear-gradient(to right, ${theme.palette.leveledBg.level2}, ${theme.palette.leveledBg.level1})`,
        padding: '1rem',
        textAlign: 'left',
      },
      'ol, ul, li': {
        textAlign: 'left',
      },
      table: {
        borderSpacing: 0,
        borderCollapse: 'collapse',
        display: 'block',
        width: 'max-content',
        maxWidth: '100%',
        overflow: 'auto',
      },
      'table th, table td': {
        padding: '6px 13px',
        border: `1px solid ${theme.palette.divider}`,
      },
      'table tr': {
        backgroundColor: `${theme.palette.background.paper}`,
        borderTop: `1px solid ${theme.palette.divider}`,
      },
    },
    code: {
      borderRadius: '5px',
      boxShadow: theme.shadows[8]
    }
  }),
)
