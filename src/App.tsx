import {DarkMode, LightMode, Logout} from '@mui/icons-material'
import {
  AppBar,
  Avatar,
  Box,
  Button,
  createTheme,
  CssBaseline,
  darkScrollbar,
  Divider,
  Grid,
  IconButton,
  IconButtonProps,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Popover,
  ThemeProvider,
  Toolbar,
  Typography,
} from '@mui/material'
import React, {useCallback, useEffect, useState} from 'react'
import {Route, Routes, useLocation, useNavigate} from 'react-router-dom'
import {axiosInstance, initAxiosInstance} from './api'
import {getUserInfo, logout} from './features/auth/userSlice'
import {initialize, switchTheme} from './features/config/configSlice'
import {RootState, useAppDispatch, useAppSelector} from './store'
import {Login} from './views/auth'
import {Notebook, NotebookAppBarWidgets} from './views/notebook'

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
    leveledBg: {
      level1: '#242424',
      level2: '#363636',
      level3: '#484848',
      level4: '#606060',
      level5: '#727272',
    },
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        body: darkScrollbar()
      },
    },
  },
});

const lightTheme = createTheme({
  palette: {
    mode: 'light',
    leveledBg: {
      level1: '#EEEEEE',
      level2: '#DDDDDD',
      level3: '#CCCCCC',
      level4: '#BBBBBB',
      level5: '#AAAAAA',
    },
  },
})

const selectTheme = (state: RootState) => state.config.theme

function MainToolbar() {
  const theme = useAppSelector(selectTheme)
  const { state: loginState, userInfo } = useAppSelector(state => state.user)
  const [anchorEl, setAnchorEl] = useState<HTMLElement | undefined>()
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const iconButtonProps: IconButtonProps = {
    size: 'large',
    edge: 'start',
    color: 'inherit',
    'aria-label': 'menu',
    sx: {mr: 2},
  }

  const handleSwitchThemeClick = useCallback(
    () => {
      dispatch(switchTheme())
    },
    [dispatch],
  )

  const handleAvatarClick = useCallback(
    e => {
      setAnchorEl(e.currentTarget);
    },
    [],
  )

  const handlePopoverClose = useCallback(
    () => {
      setAnchorEl(undefined)
    },
    [],
  )

  const handleSignInClick = useCallback(
    () => {
      navigate('/login')
      setAnchorEl(undefined)
    },
    [navigate],
  )

  const handleSignOutClick = useCallback(
    () => {
      dispatch(logout())
    },
    [dispatch],
  )

  return (
    <AppBar position={'static'}>
      <Toolbar>
        <Routes>
          <Route path={'/notebook/*'} element={<NotebookAppBarWidgets/>}/>
        </Routes>
        <Box sx={{flexGrow: 1}}/>
        <IconButton onClick={handleSwitchThemeClick} {...iconButtonProps}>
          { theme === 'dark' ? <LightMode/> : <DarkMode/> }
        </IconButton>

        <Avatar alt={userInfo?.name} src={userInfo?.avatarUrl} component={'a'} onClick={handleAvatarClick}/>
        <Popover
          id={anchorEl ? 'user-avatar-popover' : undefined}
          open={Boolean(anchorEl)}
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          onClose={handlePopoverClose}
        >
          <Box sx={{ width: 300, bgcolor: 'background.paper' }}>
            {loginState === 'online'
              ? <Typography sx={{py: '20px', pl: '10px'}}>{userInfo?.name}</Typography>
              : <Button fullWidth onClick={handleSignInClick}>Sign In</Button>
            }
            <Divider />
            {loginState === 'online'
              ? <List>
                  <ListItem disablePadding>
                    <ListItemButton onClick={handleSignOutClick}>
                      <ListItemIcon><Logout/></ListItemIcon>
                      <ListItemText primary="Sign Out" />
                    </ListItemButton>
                  </ListItem>
                </List>
              : null
            }
          </Box>
        </Popover>
      </Toolbar>
    </AppBar>
  )
}

function App() {
  const { theme, initialized } = useAppSelector(store => store.config)
  const { state: loginState, userInfo } = useAppSelector(store => store.user)
  const dispatch = useAppDispatch()
  const location = useLocation()
  const navigateTo = useNavigate()

  if (axiosInstance === undefined) initAxiosInstance()

  useEffect(() => {
    if (!initialized) {
      dispatch(initialize())
    }
    if (loginState !== 'online') {
      navigateTo('/')
    }
    if (loginState === 'online' && location.pathname === '/') {
      navigateTo('/notebook')
    }
    if (loginState === 'online' && userInfo === undefined){
      dispatch(getUserInfo())
    }
  }, [dispatch, initialized, loginState, navigateTo, userInfo, location.pathname])


  return (
    <ThemeProvider theme={theme === 'dark' ? darkTheme : lightTheme}>
      <CssBaseline enableColorScheme/>
      <Box sx={{ width: '100vw', height: '100vh' }}>
        <Grid container spacing={2} direction={'column'} sx={{ height: '100%' }}>
          <Grid item sm={'auto'} sx={{ width: '100%' }}>
            <MainToolbar/>
          </Grid>
          <Grid item sm={true} sx={{ width: '100%' }}>
            <Routes>
              <Route path='/' element={<Login/>}/>
              <Route path='/login' element={<Login/>}/>
              <Route path='/notebook' element={<Notebook/>}>
                <Route path=':pageId' element={<Notebook/>}/>
              </Route>
            </Routes>
          </Grid>
        </Grid>
      </Box>
    </ThemeProvider>
  );
}

export default App;
