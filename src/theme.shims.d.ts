declare module '@mui/material/styles' {
  interface Palette {
    leveledBg: LeveledBg;
  }

  // allow configuration using `createTheme`
  interface PaletteOptions {
    leveledBg?: LeveledBg;
  }
}

export interface LeveledBg {
  level1: string,
  level2: string,
  level3: string,
  level4: string,
  level5: string,
}
