import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {notebookService, UpdatePageDto} from '../../services/notebookService'
import {RootState} from '../../store'

export type LoadState = 'idle' | 'loading' | 'loaded' | 'failed'

export interface PageState {
  contentById: Record<number, string | undefined>
  loadStateById: Record<number, LoadState>
}

export const fetchPageContentById = createAsyncThunk<string | undefined, number, { state: RootState }>(
  'pageContent/fetchById',
  async (pageId: number) => {
    return await notebookService.getPageContent(pageId)
  },
  {
    condition: (pageId: number, { getState }) => {
      const state = getState().page.loadStateById[pageId]
      return state !== 'loading'
    }
  }
)


export const updatePageContent = createAsyncThunk<void, UpdatePageDto, { state: RootState }>(
  'pageContent/update',
  async (dto) => {
    return await notebookService.updatePageContent(dto)
  },
  {
    condition: (dto, { getState }) => {
      const contentNow = getState().page.contentById[dto.id]
      return contentNow !== dto.content && getState().page.loadStateById[dto.id] === 'loaded'
    }
  }
)

export const pageSlice = createSlice({
  name: 'page',
  initialState: {
    contentById: {},
    loadStateById: {},
    saveStateById: {},
  } as PageState,
  reducers: {
  },
  extraReducers: builder => {
    builder
      .addCase(fetchPageContentById.pending, (state: PageState, { meta }) => {
        if (state.loadStateById[meta.arg] === 'idle') {
          state.loadStateById[meta.arg] = 'loading'
        }
      })
      .addCase(fetchPageContentById.fulfilled, (state: PageState, { payload, meta }) => {
        if (payload !== undefined) {
          state.contentById[meta.arg] = payload
          state.loadStateById[meta.arg] = 'loaded'
        }
      })
      .addCase(fetchPageContentById.rejected, (state: PageState, { meta }) => {
        state.loadStateById[meta.arg] = 'failed'
      })
      .addCase(updatePageContent.fulfilled, (state: PageState, { payload, meta }) => {
        state.loadStateById[meta.arg.id] = 'loaded'
        state.contentById[meta.arg.id] = meta.arg.content
      })
  }
})

// export const { } = pageSlice.actions

export const pageReducer = pageSlice.reducer
