import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {Book, CreatePageDto, notebookService, Page, RenameBookDto, RenamePageDto} from '../../services/notebookService'
import {RootState} from '../../store'

export interface BookState {
  books: Book[]
  loadState: 'notLoaded' | 'loading' | 'loaded'
}

export const fetchBooks = createAsyncThunk<Book[], void, { state: RootState }>(
  'books/fetch',
  async () => {
    return await notebookService.getAllBooks()
  },
  {
    condition: (_, { getState }) => {
      return getState().book.loadState !== 'loading'
    }
  }
)

export const createNewBook = createAsyncThunk<Book, string, { state: RootState }>(
  'book/create',
  async (title) => {
    return await notebookService.createBook({
      title
    })
  }
)

export const createNewPage = createAsyncThunk<Page, CreatePageDto, { state: RootState }>(
  'page/create',
  async (dto) => {
    return await notebookService.createPage(dto)
  }
)

export const deleteBook = createAsyncThunk<void, number, { state: RootState }>(
  'book/delete',
  async (bookId) => {
    return await notebookService.deleteBook(bookId)
  }
)

export const deletePage = createAsyncThunk<void, number, { state: RootState }>(
  'page/delete',
  async (pageId) => {
    return await notebookService.deletePage(pageId)
  }
)

export const renameBook = createAsyncThunk<void, RenameBookDto, { state: RootState }>(
  'book/rename',
  async (dto) => {
    return await notebookService.renameBook(dto)
  }
)

export const renamePage = createAsyncThunk<void, RenamePageDto, { state: RootState }>(
  'page/rename',
  async (dto) => {
    return await notebookService.renamePage(dto)
  }
)

export const bookSlice = createSlice({
  name: 'book',
  initialState: {
    books: [],
    loadState: 'notLoaded',
  } as BookState,
  reducers: {

  },
  extraReducers: builder => {
    builder
      .addCase(fetchBooks.pending, (state: BookState) => {
        if (state.loadState === 'notLoaded') {
          state.loadState = 'loading'
        }
      })
      .addCase(fetchBooks.fulfilled, (state: BookState, { payload }) => {
        state.books = payload
        state.loadState = 'loaded'
      })
      .addCase(fetchBooks.rejected, (state: BookState, { error }) => {
        console.error(error)
        state.loadState = 'notLoaded'
      })
      .addCase(createNewBook.fulfilled, (state: BookState, { payload }) => {
        state.books = [...state.books, payload]
      })
      .addCase(createNewPage.fulfilled, (state: BookState, { meta, payload }) => {
        const book = state.books.find(b => b.id === meta.arg.bookId)
        if (book) {
          book.pages = [...book.pages, payload]
        }
      })
      .addCase(renameBook.fulfilled, (state: BookState, { meta }) => {
        const book = state.books.find(b => b.id === meta.arg.id)
        if (book) {
          book.title = meta.arg.title
        }
      })
      .addCase(renamePage.fulfilled, (state: BookState, { meta }) => {
        const page = state.books.flatMap(b => b.pages).find(p => p.id === meta.arg.id)
        if (page) {
          page.title = meta.arg.title
        }
      })
      .addCase(deleteBook.fulfilled, (state: BookState, { meta }) => {
        state.books = state.books.filter(b => b.id !== meta.arg)
      })
      .addCase(deletePage.fulfilled, (state: BookState, { meta }) => {
        const book = state.books.find(b => b.pages.find(p => p.id === meta.arg))
        if (book) {
          book.pages = book.pages.filter(p => p.id !== meta.arg)
        }
      })
  }
})

// export const {  } = bookSlice.actions

export const bookReducer = bookSlice.reducer
