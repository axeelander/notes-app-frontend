import {createSlice, PayloadAction} from '@reduxjs/toolkit'

export interface AppConfig {
  initialized: boolean
  theme: 'dark' | 'light'
  showNotebookListPanel: boolean
  editorMode: 'text' | 'preview' | 'both'
}

function saveConfig(config: AppConfig) {
  localStorage.setItem('config', JSON.stringify({
    theme: config.theme,
    showNotebookListPanel: config.showNotebookListPanel,
    editorMode: config.editorMode,
  }))
}

export const configSlice = createSlice({
  name: 'config',
  initialState: {
    initialized: false,
    theme: 'dark',
    showNotebookListPanel: true,
    editorMode: 'text',
  } as AppConfig,
  reducers: {
    initialize: state => {
      if (!state.initialized) {
        const configJson = localStorage.getItem('config')
        if (configJson) {
          Object.assign(state, JSON.parse(configJson))
        }
        state.initialized = true
      }
    },
    switchTheme: state => {
      state.theme = state.theme === 'dark' ? 'light' : 'dark'
      saveConfig(state)
    },
    switchNotebookListPanel: state => {
      state.showNotebookListPanel = !state.showNotebookListPanel
      saveConfig(state)
    },
    changeEditorMode: (state, { payload }: PayloadAction<AppConfig['editorMode']>) => {
      state.editorMode = payload
      saveConfig(state)
    }
  },
})

export const { initialize, switchTheme, switchNotebookListPanel, changeEditorMode } = configSlice.actions

export const configReducer = configSlice.reducer
