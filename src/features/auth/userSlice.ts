import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {LoginReqDto, RegisterReqDto, UserInfo, userService} from '../../services/userService'
import {RootState} from '../../store'

export interface UserState {
  state: 'offline' | 'logging in' |'online'
  userInfo?: UserInfo
}

export const login = createAsyncThunk<UserInfo, LoginReqDto, { state: RootState }>(
  'user/login',
  async (dto) => {
    return await userService.login(dto)
  },
  {
    condition: (_, { getState }) => {
      return getState().user.state !== 'logging in'
    }
  }
)

export const logout = createAsyncThunk<void, void, { state: RootState }>(
  'user/logout',
  async () => {
    return await userService.logout()
  },
  {
    condition: (_, { getState }) => {
      return getState().user.state === 'online'
    }
  }
)

export const register = createAsyncThunk<UserInfo, RegisterReqDto, { state: RootState }>(
  'user/register',
  async (dto) => {
    return await userService.register(dto)
  },
  {
    condition: (_, { getState }) => {
      return getState().user.state !== 'logging in'
    }
  }
)

export const getUserInfo = createAsyncThunk<UserInfo, void, { state: RootState }>(
  'user/getInfo',
  async () => {
    return await userService.getUserInfo()
  }
)

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    state: 'online',
    userInfo: undefined,
  } as UserState,
  reducers: {
  },
  extraReducers: builder => {
    builder
      .addCase(login.pending, (state: UserState) => {
        if (state.state === 'offline') {
          state.state = 'logging in'
        }
      })
      .addCase(login.fulfilled, (state: UserState, { payload }) => {
        state.state = 'online'
        state.userInfo = payload
      })
      .addCase(login.rejected, (state: UserState, { error }) => {
        state.state = 'offline'
        throw error.message
      })
      .addCase(logout.fulfilled, (state: UserState) => {
        state.userInfo = undefined
        state.state = 'offline'
      })
      .addCase(register.pending, (state: UserState) => {
        if (state.state === 'offline') {
          state.state = 'logging in'
        }
      })
      .addCase(register.fulfilled, (state: UserState, { payload }) => {
        state.state = 'online'
        state.userInfo = payload
      })
      .addCase(register.rejected, (state: UserState, { error }) => {
        state.state = 'offline'
        throw error.message
      })
      .addCase(getUserInfo.fulfilled, (state: UserState, {payload}) => {
        state.state = 'online'
        state.userInfo = payload
      })
  }
})

// export const { } = userSlice.actions

export const userReducer = userSlice.reducer
