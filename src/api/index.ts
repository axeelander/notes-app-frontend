import axios, {AxiosError, AxiosInstance} from 'axios'
import {logout} from '../features/auth/userSlice'
import {
  Book,
  CreateBookDto,
  CreatePageDto,
  Page,
  RenameBookDto,
  RenamePageDto,
  UpdatePageDto,
} from '../services/notebookService'
import {LoginReqDto, LoginResDto, RegisterReqDto, UserInfo} from '../services/userService'
import {store} from '../store'

export function initAxiosInstance() {
  let instance = axios.create({
    baseURL: process.env['REACT_APP_BASE_URL'],
  })

  let token = localStorage.getItem("authToken")
  if (token) {
    instance.defaults.headers.common["Authorization"] = `Bearer ${token}`
  }

  instance.interceptors.response.use(res => {
    return res
  }, (err: AxiosError) => {
    let status = err.response?.status
    if (status === 401) {
      store.dispatch(logout())
    } else if (status === 400) {
      throw err.response?.data
    }
  })

  axiosInstance = instance
}

export let axiosInstance: AxiosInstance | undefined = undefined

function createAPI<T extends (...args: any[]) => any>(action: T): T {
  // @ts-ignore
  return (...args) => {
    if (axiosInstance === undefined) {
      throw new Error('Axios hasn\'t initialized')
    }
    return action(...args)
  }
}

export const registerAPI = createAPI(async (dto: RegisterReqDto) => {
  return (await axiosInstance!.post('Auth/register', dto)).data as UserInfo
})

export const loginAPI = createAPI(async (dto: LoginReqDto) => {
  return (await axiosInstance!.post('Auth/login', dto)).data as LoginResDto
})

export const getUserInfoAPI = createAPI(async () => {
  return (await axiosInstance!.get('Auth/userinfo')).data as UserInfo
})

export const createBookAPI = createAPI(async  (dto: CreateBookDto) => {
  return (await axiosInstance!.post('Notebook/book/new', dto)).data as Book
})

export const deleteBookAPI = createAPI(async  (dto: { id: number }) => {
  await axiosInstance!.delete('Notebook/book', {
    data: dto
  })
})

export const renameBookAPI = createAPI(async  (dto: RenameBookDto) => {
  return (await axiosInstance!.put('Notebook/book/rename', dto)).data as Book
})

export const getBooksAPI = createAPI(async  () => {
  return (await axiosInstance!.get('Notebook/books')).data as Book[]
})

export const createPageAPI = createAPI(async  (dto: CreatePageDto) => {
  return (await axiosInstance!.post('Notebook/page/new', dto)).data as Page
})

export const deletePageAPI = createAPI(async  (dto: { id: number }) => {
  await axiosInstance!.delete('Notebook/page', {
    data: dto
  })
})

export const renamePageAPI = createAPI(async  (dto: RenamePageDto) => {
  return (await axiosInstance!.put('Notebook/page/rename', dto)).data as Page
})

export const getPageContentAPI = createAPI(async  (id: number) => {
  return (await axiosInstance!.get(`Notebook/page/${id}`)).data as {
    content: string
  }
})

export const updatePageAPI = createAPI(async  (dto: UpdatePageDto) => {
  await axiosInstance!.put('Notebook/page/update', dto)
})
